#include <iostream>
#include "queue.h"
#define SIZE 2
#define FIRST_ELEMENT 1
#define SECOND_ELEMENT 2
#define THEIRD_ELEMENT 3
void main()
{
	//creating a queue
	queue* q = new queue;
	initQueue(q,SIZE);
	
	//DEMO:

	//Adding elements to the queue
	enqueue(q, FIRST_ELEMENT);
	enqueue(q, SECOND_ELEMENT);
	enqueue(q, THEIRD_ELEMENT);

	//PRINTER
	queuePrinter(q);

	//Removing an element from the queue
	dequeue(q);

	//PRINTER
	queuePrinter(q);

	//cleaning the queue
	cleanQueue(q);

	//Adding elements to the queue
	enqueue(q, THEIRD_ELEMENT);
	enqueue(q, THEIRD_ELEMENT);
	enqueue(q, THEIRD_ELEMENT);

	//PRINTER
	queuePrinter(q);

	delete(q);
}