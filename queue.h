#ifndef QUEUE_H
#define QUEUE_H
#define EMPTY -1

/*
The queue contains:

- array -> The array that contains all of the elements 
- front -> The index of the front of the queue
- rear -> The index of the back of the queue
- size -> The size of the queue 
*/
typedef struct queue
{
	unsigned int* array;
	int front, rear;
	unsigned int size;
} queue;

//This function checks if the index of the front of the queue is the same as the index of the back of the queue and returns a bool value
bool isFull(queue* q)
{
	return q->rear == q->size;
}

//This function checks if the index of the front and back of the queue is equal to -1 which means that it's empty 
bool isEmpty(queue* q)
{
	return q->front == EMPTY && q->rear == EMPTY;
}

//This function creates the queue 
void initQueue(queue* q, unsigned int size)
{
	q->size = size;
	q->front = EMPTY;
	q->rear = EMPTY;
	q->array = new unsigned int[(size * sizeof(unsigned int))];
}

//This function cleans the queue
void cleanQueue(queue* q)
{
	q->front =  q->rear = EMPTY;
}

//This function adds a new element to the queue
void enqueue(queue* q, unsigned int newValue)
{
	//Checking if the queue is full
	if (!isFull(q))
	{
		//checking if the front was empty;
		if (q->front == EMPTY)
		{
			q->front = 0;
		}
		//increasing the index of the back of the line
		q->rear++;

		//adding the elemnt to the queue
		q->array[q->rear] = newValue;
	}
}

int dequeue(queue* q) // return element in top of queue, or -1 if empty
{
	//The item that we return at the end
	int item = 0;

	//In case the queue is empty
	if (isEmpty(q))
	{
		item = EMPTY;
	}

	//Checking if the front item is the last item
	else if (q->front == q->rear)
	{
		item = q->array[q->front];
		q->front = q->rear = EMPTY;
	}
	else
	{
		//Putting the item in the front inside the returning item
		item = q->array[q->front];
		//increasing the index of the back of the line
		q->front++;
	}
	return item;
}

//function to print the queue and test it.
void queuePrinter(queue* q)
{
	int i = 0;
	if (!isEmpty(q))
	{
		printf("\nThe queue:");
		for (i = q->front; i <= q->rear; i++)
		{
			printf ("\n%d\n", q->array[i]);
		}
	}
}
#endif /* QUEUE_H */