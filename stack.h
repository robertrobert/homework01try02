#ifndef STACK_H
#define STACK_H
#include "linkedList.h"
/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	unsigned int element;
	stack* pre;
	stack* next;
} stack;

stack *top = NULL;

void push(stack* s, unsigned int element)
{
	s->pre = top;
	s->element = element;
	s->next = top;
	top = s;
}
int pop(stack* s)
{
	s = s->pre;
	return 0;
}
void initStack(stack* s)
{
	s = new stack();
	s->pre = new stack();
}
void cleanStack(stack* s);

#endif // STACK_H