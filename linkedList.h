#ifndef LINKED_H
#define LINKED_H
typedef struct linkedList
{
	unsigned int data;
	struct linkedList* next;
} linkedList;
#endif